<?php
namespace Avris\Micrus\Forms\Assert;

abstract class Assert
{
    /** @var string */
    protected $message;

    public function __construct($message = false)
    {
        $this->message = $message;
    }

    /**
     * @param $value
     * @return true|string (returns true when valid, string with error message otherwise)
     */
    abstract public function validate($value);

    /**
     * @return array
     */
    public function getHtmlAttributes()
    {
        return [];
    }

    /**
     * @param string $value
     * @return bool
     */
    public static function isEmpty($value)
    {
        return $value === "" || $value === null || $value === false ||
            (is_array($value) && !count($value));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return str_replace('\\', '.', preg_replace('#^.*\\\\Assert\\\\(.*)#U', '$1', get_class($this)));
    }

    /**
     * @return array
     */
    public function getReplacements()
    {
        return [];
    }
}
