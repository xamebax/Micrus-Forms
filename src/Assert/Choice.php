<?php
namespace Avris\Micrus\Forms\Assert;

class Choice extends Assert
{
    /** @var array */
    protected $choices;

    /** @var bool */
    protected $multiple;

    /**
     * @param array $choices
     * @param bool $multiple
     * @param string|false $message
     */
    public function __construct($choices, $multiple, $message = false)
    {
        $this->choices = $choices;
        $this->multiple = $multiple;
        parent::__construct($message);
    }

    public function validate($value)
    {
        if (!$this->multiple && is_array($value)) {
            return $this->message;
        }

        if ($this->multiple && !is_array($value)) {
            return $this->message;
        }

        if (!is_array($value)) {
            $value = [$value];
        }

        $allowed = array_keys($this->choices);
        foreach ($value as $singleValue) {
            if (!in_array($singleValue, $allowed)) {
                return $this->message;
            }
        }

        return true;
    }
}
