<?php
namespace Avris\Micrus\Forms\Assert;

class Contains extends Assert
{
    /** @var string */
    protected $required;

    public function __construct($required, $message = false)
    {
        $this->required = $required;
        parent::__construct($message);
    }

    /**
     * @param $value
     * @return true|string (returns true when valid, string with error message otherwise)
     */
    public function validate($value)
    {
        return mb_strpos($value, $this->required) === false ? $this->message : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->required];
    }
}
