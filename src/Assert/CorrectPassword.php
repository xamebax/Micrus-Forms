<?php
namespace Avris\Micrus\Forms\Assert;

use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\Security\CryptInterface;
use Avris\Micrus\Tool\Security\SecurityManager;

class CorrectPassword extends Assert implements IsRequired
{
    /** @var callable */
    protected $callback;

    /** @var CryptInterface */
    protected $crypt;

    public function __construct($callback, CryptInterface $crypt, $message = false)
    {
        $this->callback = $callback;
        $this->crypt = $crypt;
        parent::__construct($message);
    }

    public function validate($value)
    {
        /** @var UserInterface $dbUser */
        $dbUser = call_user_func($this->callback);

        if (!$dbUser) {
            return $this->message;
        }

        $auths = $dbUser->getAuthenticators(SecurityManager::AUTHENTICATOR_PASSWORD);

        foreach ($auths as $auth) {
            if ($this->crypt->validate($value, $auth->getPayload())) {
                return true;
            }
        }

        return $this->message;
    }
}
