<?php
namespace Avris\Micrus\Forms\Assert;

class Csrf extends Assert implements IsRequired
{
    public function validate($value)
    {
        return $value === $_SESSION['_csrf'] ? true : $this->message;
    }
}
