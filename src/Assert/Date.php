<?php
namespace Avris\Micrus\Forms\Assert;

class Date extends Assert
{
    public function validate($value)
    {
        try {
            $obj = new \DateTime($value);
        } catch (\Exception $e) {
            return $this->message;
        }

        if ($obj->format('Y-m-d') !== $value) {
            return $this->message;
        }

        return true;
    }

    public function getHtmlAttributes()
    {
        return ['pattern="^\d{4}-\d{2}-\d{2}$"'];
    }
}
