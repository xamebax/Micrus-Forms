<?php
namespace Avris\Micrus\Forms\Assert;

class DateTime extends Assert
{
    public function validate($value)
    {
        $value = str_replace('T', ' ', $value);

        try {
            $obj = new \DateTime($value);
        } catch (\Exception $e) {
            return $this->message;
        }

        if ($obj->format('Y-m-d H:i') !== $value && $obj->format('Y-m-d H:i:s') !== $value) {
            return $this->message;
        }

        return true;
    }

    public function getHtmlAttributes()
    {
        return [
            'pattern="^\d{4}-\d{2}-\d{2} \d{2}:\d{2}$"',
        ];
    }
}
