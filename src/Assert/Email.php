<?php
namespace Avris\Micrus\Forms\Assert;

class Email extends Assert
{
    public function validate($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) ? true : $this->message;
    }
}
