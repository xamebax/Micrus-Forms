<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\Assert;

class Extension extends Assert
{
    protected $extension;

    public function __construct($extension, $message = false)
    {
        $this->extension = is_array($extension) ? $extension : [$extension];
        $this->message = $message;
    }

    public function validate($value)
    {
        /** @var UploadedFile $value */
        return in_array($value->getExtension(), $this->extension)
            ? true
            : $this->message;
    }
}
