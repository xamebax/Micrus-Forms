<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\Assert;

class File extends Assert
{
    public function __construct($message = false)
    {
        $this->message = $message;
    }

    public function validate($value)
    {
        return $value instanceof UploadedFile && !$value->getError()
            ? true
            : $this->message;
    }
}
