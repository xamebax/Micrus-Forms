<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\Assert;

class Image extends Assert
{
    public function __construct($message = false)
    {
        $this->message = $message;
    }

    public function validate($value)
    {
        /** @var UploadedFile $value */
        return is_numeric(exif_imagetype($value->getTmpName()))
            ? true
            : $this->message;
    }
}
