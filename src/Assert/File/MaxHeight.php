<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\Assert;

class MaxHeight extends Assert
{
    protected $max;

    public function __construct($max, $message = false)
    {
        $this->max = $max;
        $this->message = $message;
    }

    public function validate($value)
    {
        /** @var UploadedFile $value */
        $imgsize = getimagesize($value->getTmpName());
        return $imgsize[1] > $this->max ? $this->message : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->max];
    }
}
