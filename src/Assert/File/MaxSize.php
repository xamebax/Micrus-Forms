<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Forms\Assert\Assert;

class MaxSize extends Assert
{
    protected $max;

    protected static $units = ['B', 'kB', 'MB', 'GB', 'TB'];

    protected static $unitsPrefixes = [
        '' => 0,
        'b' => 0,
        'k' => 1,
        'm' => 2,
        'g' => 3,
        't' => 4,
    ];

    public function __construct($max, $message = false)
    {
        $this->max = $this->parseSize($max);
        $this->message = $message;
    }

    public function validate($value)
    {
        /** @var UploadedFile $value */
        return $value->getSize() > $this->max ? $this->message : true;
    }

    protected function parseSize($size)
    {
        if (is_int($size)) {
            return $size;
        }

        if (!preg_match('/^(\d+\.?\d*)\s*([kmgtb]?)b?$/i', $size, $matches)) {
            throw new InvalidArgumentException(sprintf('Cannot parse file size "%s"', $size));
        }

        $power = self::$unitsPrefixes[strtolower($matches[2])];
        $bytes = (float)$matches[1] * pow(1024, $power);

        return (int)$bytes;
    }

    protected function displaySize($size)
    {
        $i = -1;

        do {
            $oldSize = $size;
            $size = $oldSize / 1024;
            $i++;
        } while ($size > 0.1);

        return sprintf('%.1f %s', $oldSize, self::$units[$i]);
    }

    public function getReplacements()
    {
        return ['%value%' => $this->displaySize($this->max)];
    }
}
