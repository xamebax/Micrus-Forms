<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\Assert;

class MinHeight extends Assert
{
    protected $min;

    public function __construct($min, $message = false)
    {
        $this->min = $min;
        $this->message = $message;
    }

    public function validate($value)
    {
        /** @var UploadedFile $value */
        $imgsize = getimagesize($value->getTmpName());
        return $imgsize[1] < $this->min ? $this->message : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->min];
    }
}
