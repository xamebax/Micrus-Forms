<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\Assert;

class Ratio extends Assert
{
    const HORIZONTAL = 'horizontal';
    const VERTICAL = 'vertical';
    const SQUARE = 'square';

    protected $ratio;

    public function __construct($ratio, $message = false)
    {
        $this->ratio = $ratio;
        $this->message = $message;
    }

    public function validate($value)
    {
        /** @var UploadedFile $value */
        $imgsize = getimagesize($value->getTmpName());

        return ($this->ratio == self::HORIZONTAL && $imgsize[0] < $imgsize[1]) ||
            ($this->ratio == self::VERTICAL && $imgsize[0] > $imgsize[1]) ||
            ($this->ratio == self::SQUARE && $imgsize[0] != $imgsize[1])
                ? $this->message
                : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->ratio];
    }
}
