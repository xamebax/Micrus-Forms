<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Bag\BagHelper;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\Assert;
use Avris\Bag\Bag;

class Type extends Assert
{
    protected $type;

    public function __construct($type, $message = false)
    {
        $this->type = $type;
        $this->message = $message;
    }

    public function validate($value)
    {
        /** @var UploadedFile $value */
        return in_array($value->getClientMediaType(), BagHelper::toArray($this->type))
            ? true
            : $this->message;
    }
}
