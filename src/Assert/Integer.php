<?php
namespace Avris\Micrus\Forms\Assert;

class Integer extends Assert
{
    public function validate($value)
    {
        return is_numeric($value) && round($value) == $value ? true : $this->message;
    }
}
