<?php
namespace Avris\Micrus\Forms\Assert;

class MaxCount extends Assert
{
    protected $max;

    public function __construct($max, $message = false)
    {
        $this->max = $max;
        parent::__construct($message);
    }

    public function validate($value)
    {
        return count($value) > $this->max ? $this->message : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->max];
    }
}
