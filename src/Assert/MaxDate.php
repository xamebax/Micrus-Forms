<?php
namespace Avris\Micrus\Forms\Assert;

class MaxDate extends Assert
{
    protected $max;

    public function __construct($max, $message = false)
    {
        $this->max = $max instanceof \DateTime ? $max : new \DateTime($max);
        parent::__construct($message);
    }

    public function validate($value)
    {
        if (!$value instanceof \DateTime) {
            $value = new \DateTime($value);
        }

        return $value > $this->max ? $this->message : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->max->format('Y-m-d')];
    }
}
