<?php
namespace Avris\Micrus\Forms\Assert;

class MaxLength extends Assert
{
    protected $max;

    public function __construct($max, $message = false)
    {
        $this->max = $max;
        parent::__construct($message);
    }

    public function validate($value)
    {
        return mb_strlen($value) > $this->max ? $this->message : true;
    }

    public function getHtmlAttributes()
    {
        return ['maxLength="' . htmlentities($this->max) . '"'];
    }

    public function getReplacements()
    {
        return ['%value%' => $this->max];
    }
}
