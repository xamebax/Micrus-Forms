<?php
namespace Avris\Micrus\Forms\Assert;

class Min extends Assert
{
    protected $min;

    public function __construct($min, $message = false)
    {
        $this->min = $min;
        parent::__construct($message);
    }

    public function validate($value)
    {
        return $value < $this->min ? $this->message : true;
    }

    public function getHtmlAttributes()
    {
        return ['min="' . htmlentities($this->min) . '"'];
    }

    public function getReplacements()
    {
        return ['%value%' => $this->min];
    }
}
