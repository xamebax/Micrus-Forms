<?php
namespace Avris\Micrus\Forms\Assert;

class MinCount extends Assert
{
    protected $min;

    public function __construct($min, $message = false)
    {
        $this->min = $min;
        parent::__construct($message);
    }

    public function validate($value)
    {
        return count($value) < $this->min ? $this->message : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->min];
    }
}
