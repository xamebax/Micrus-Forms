<?php
namespace Avris\Micrus\Forms\Assert;

class MinDate extends Assert
{
    protected $min;

    public function __construct($min, $message = false)
    {
        $this->min = $min instanceof \DateTime ? $min : new \DateTime($min);
        parent::__construct($message);
    }

    public function validate($value)
    {
        if (!$value instanceof \DateTime) {
            $value = new \DateTime($value);
        }

        return $value < $this->min ? $this->message : true;
    }

    public function getReplacements()
    {
        return ['%value%' => $this->min->format('Y-m-d')];
    }
}
