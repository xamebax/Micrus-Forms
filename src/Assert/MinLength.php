<?php
namespace Avris\Micrus\Forms\Assert;

class MinLength extends Assert
{
    protected $min;

    public function __construct($min, $message = false)
    {
        $this->min = $min;
        parent::__construct($message);
    }

    public function validate($value)
    {
        return mb_strlen($value) < $this->min ? $this->message : true;
    }

    public function getHtmlAttributes()
    {
        return [
            'minLength="' . htmlentities($this->min) . '"',
        ];
    }

    public function getReplacements()
    {
        return ['%value%' => $this->min];
    }
}
