<?php
namespace Avris\Micrus\Forms\Assert;

class NotBlank extends Assert implements IsRequired
{
    public function validate($value)
    {
        return $this->isEmpty($value) ? $this->message : true;
    }

    public function getHtmlAttributes()
    {
        return ['required'];
    }
}
