<?php
namespace Avris\Micrus\Forms\Assert;

class Number extends Assert
{
    public function validate($value)
    {
        return is_numeric($value) ? true : $this->message;
    }
}
