<?php
namespace Avris\Micrus\Forms\Assert;

use Avris\Micrus\Forms\FormObject;

class ObjectValidator extends Assert implements IsRequired
{
    /** @var callable */
    protected $callback;

    /** @var FormObject */
    protected $object;

    public function __construct($callback, $object, $message = false)
    {
        $this->callback = $callback;
        $this->object = $object;
        parent::__construct($message);
    }

    public function validate($value)
    {
        return call_user_func($this->callback, $this->object);
    }
}
