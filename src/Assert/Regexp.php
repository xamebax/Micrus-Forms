<?php
namespace Avris\Micrus\Forms\Assert;

class Regexp extends Assert
{
    /** @var string */
    protected $pattern;

    /** @var string */
    protected $switches;

    public function __construct($pattern, $message = false, $switches = 'i')
    {
        $this->pattern = $pattern;
        parent::__construct($message);
        $this->switches = $switches;
    }

    public function validate($value)
    {
        return preg_match('#'.$this->pattern.'#'.$this->switches, $value) ? true : $this->message;
    }

    public function getHtmlAttributes()
    {
        return [
            'pattern="' . htmlentities($this->pattern) . '"',
            'title="' . htmlentities(
                $this->message ?: l('validator.' . $this->getName(), $this->getReplacements())
            ) . '"',
        ];
    }

    public function getReplacements()
    {
        return ['%pattern%' => $this->pattern];
    }
}
