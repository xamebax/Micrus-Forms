<?php
namespace Avris\Micrus\Forms\Assert;

class Step extends Assert
{
    protected $step;

    public function __construct($step, $message = false)
    {
        $this->step = $step;
        parent::__construct($message);
    }

    public function validate($value)
    {
        return abs(($value / $this->step) - round($value / $this->step, 0)) > 0.0001 ? $this->message : true;
    }

    public function getHtmlAttributes()
    {
        return [
            'step="' . htmlentities($this->step) . '"',
        ];
    }

    public function getReplacements()
    {
        return ['%value%' => $this->step];
    }
}
