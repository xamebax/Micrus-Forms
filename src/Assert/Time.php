<?php
namespace Avris\Micrus\Forms\Assert;

class Time extends Assert
{
    public function validate($value)
    {
        try {
            $obj = new \DateTime($value);
        } catch (\Exception $e) {
            return $this->message;
        }

        if ($obj->format('H:i') !== $value) {
            return $this->message;
        }

        return true;
    }

    public function getHtmlAttributes()
    {
        return [
            'pattern="^\d{2}:\d{2}$"',
        ];
    }
}
