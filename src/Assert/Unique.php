<?php
namespace Avris\Micrus\Forms\Assert;

use Avris\Micrus\Forms\FormObject;
use Avris\Micrus\Model\ORM;

class Unique extends Assert
{
    /** @var  ORM */
    protected $orm;

    /** @var FormObject */
    protected $object;

    /** @var string */
    protected $type;

    /** @var string */
    protected $field;

    public function __construct(ORM $orm, FormObject $object, $type, $field, $message = false)
    {
        $this->orm = $orm;
        $this->object = $object;
        $this->type = $type;
        $this->field = $field;
        parent::__construct($message);
    }

    public function validate($value)
    {
        $duplicates = $this->orm->findBy($this->type, $this->field, $value);

        foreach ($duplicates as $duplicate) {
            if (FormObject::get($duplicate, 'id') !== $this->object->id) {
                return $this->message;
            }
        }

        return true;
    }
}
