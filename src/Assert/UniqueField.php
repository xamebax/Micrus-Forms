<?php
namespace Avris\Micrus\Forms\Assert;

class UniqueField extends Assert
{
    /** @var string */
    protected $field;

    public function __construct($field, $message = false)
    {
        $this->field = $field;
        parent::__construct($message);
    }

    /**
     * @param $value
     * @return true|string (returns true when valid, string with error message otherwise)
     */
    public function validate($value)
    {
        $found = [];

        foreach ($value as $singleValue) {
            if (empty($singleValue[$this->field])) {
                continue;
            }

            if (isset($found[$singleValue[$this->field]])) {
                return $this->message;
            }

            $found[$singleValue[$this->field]] = true;
        }

        return true;
    }

    public function getReplacements()
    {
        return ['%field%' => $this->field];
    }
}
