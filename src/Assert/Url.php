<?php
namespace Avris\Micrus\Forms\Assert;

class Url extends Assert
{
    public function validate($value)
    {
        if (!filter_var($value, FILTER_VALIDATE_URL)) {
            return $this->message;
        }

        return true;
    }
}
