<?php
namespace Avris\Micrus\Forms;

use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Forms\Widget\DontMapOnObject;
use Avris\Micrus\Forms\Widget\File;
use Avris\Micrus\Forms\Widget\BindableWidgetInterface;
use Avris\Micrus\Forms\Widget\WhenNotSetInRequest;
use Avris\Micrus\Forms\Widget\Widget;
use Avris\Micrus\Forms\Assert\Assert;
use Avris\Bag\Bag;
use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Forms\Style\FormStyle;

abstract class Form
{
    /** @var FormObject */
    protected $object;

    /** @var array */
    protected $options;

    /** @var Widget[][] */
    protected $fields = [];

    /** @var string */
    protected $currentSection = '';

    /** @var bool */
    protected $bound = false;

    /** @var Bag|string[] */
    protected $rawValues;

    /** @var ContainerInterface */
    protected $container;

    /**
     * @param object $object
     * @param ContainerInterface $container
     * @param array $options
     */
    public function __construct($object = null, ContainerInterface $container = null, $options = [])
    {
        $this->object = new FormObject($object);
        $this->container = $container;
        $this->options = new Bag($options);
        $this->rawValues = new Bag();
        $this->setCsrfToken();
        $this->configure();
    }

    protected function setCsrfToken()
    {
        if (($this->options->get('csrf', true)) && isset($_SESSION['_csrf'])) {
            if (!$this->get('_csrf')) {
                $this->add('_csrf', 'Csrf');
            }
            $this->rawValues->set('_csrf', $_SESSION['_csrf']);
        }
    }

    abstract protected function configure();

    /**
     * @param string $section
     * @return $this
     */
    public function with($section)
    {
        $this->currentSection = (string) $section;

        return $this;
    }

    /**
     * @return $this
     */
    public function end()
    {
        $this->currentSection = '';

        return $this;
    }

    /**
     * @param string $name
     * @param string $type
     * @param array $options
     * @param Assert[] $asserts
     * @param bool $visible
     * @return $this
     * @throws InvalidArgumentException
     */
    public function add($name, $type = 'Text', $options = [], $asserts = [], $visible = true)
    {
        return $this->addWidget($this->createWidget($name, $type, $options, $asserts), $visible);
    }

    /**
     * @param Widget $widget
     * @param bool $visible
     * @return $this
     * @throws InvalidArgumentException
     */
    public function addWidget(Widget $widget, $visible = true)
    {
        if (!$visible) {
            return $this;
        }

        $name = $widget->getName();

        if ($this->get($name)) {
            throw new InvalidArgumentException(sprintf('Cannot redefine widget "%s"', $name));
        }

        if (!isset($this->fields[$this->currentSection])) {
            $this->fields[$this->currentSection] = [];
        }

        $this->fields[$this->currentSection][$name] = $widget;

        return $this;
    }

    /**
     * @param string $name
     * @param string $type
     * @param array $options
     * @param array $asserts
     * @return Widget
     * @throws InvalidArgumentException
     */
    public function createWidget($name, $type = 'Text', $options = [], $asserts = [])
    {
        $class = $this->buildWidgetClass($type);

        if (!is_array($asserts)) {
            $asserts = [$asserts];
        }

        return new $class($this, $name, $options, $asserts);
    }

    /**
     * @param string $type
     * @return string
     * @throws InvalidArgumentException
     */
    protected function buildWidgetClass($type)
    {
        if (strpos($type, '\\') !== false && class_exists($type)) {
            return $type;
        }

        $type = $type ? ucfirst($type) : 'Text';

        if (class_exists($class = 'App\\Widget\\' . $type)) {
            return $class;
        }

        if (class_exists($class = 'Avris\\Micrus\\Forms\\Widget\\' . $type)) {
            return $class;
        }

        throw new InvalidArgumentException(sprintf('Widget of type %s not found', $type));
    }

    /**
     * @param bool|true $original
     * @return FormObject|object|null
     */
    public function getObject($original = true)
    {
        return $original ? $this->object->getOriginal() : $this->object;
    }

    /**
     * @param string|null $key
     * @return array|mixed
     */
    public function getOptions($key = null)
    {
        return $key ? $this->options->get($key) : $this->options;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getRawValue($name)
    {
        return $this->rawValues->get($name, false);
    }

    /**
     * @param string $name
     * @return Widget|null
     */
    public function get($name)
    {
        foreach ($this->fields as $section) {
            if (isset($section[$name])) {
                return $section[$name];
            }
        }

        return null;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        return $this->get($name) instanceof Widget;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return isset($this->options['name']) ? $this->options['name'] : $this->object->getName();
    }

    public function getModelName()
    {
        return $this->object->getName();
    }

    public function __toString()
    {
        return $this->render();
    }

    /**
     * @param FormStyle|string|null $style
     * @param string $wrapFieldBefore
     * @param string $wrapFieldAfter
     * @return string
     */
    public function render($style = null, $wrapFieldBefore = '', $wrapFieldAfter = '')
    {
        if (is_string($style)) {
            $style = new $style;
        }

        $out = '';
        foreach ($this->fields as $section => $fields) {
            if ($section) {
                $out .= '<fieldset><legend>'.$section.'</legend>';
            }
            foreach ($fields as $field) {
                $out .=  $wrapFieldBefore . $field->render($style) . $wrapFieldAfter;
            }
            if ($section) {
                $out .= '</fieldset>';
            }
        }
        return $out;
    }

    /**
     * @param RequestInterface $request
     */
    public function bindRequest(RequestInterface $request)
    {
        if ($request->isPost()) {
            $this->bind($request->getData(), $request->getFiles());
        }
    }

    /**
     * @param $data
     * @param UploadedFile[] $files
     */
    public function bind($data, $files = [])
    {
        if (!count($data) || !isset($data[$this->getName()])) {
            return;
        }

        $data = $data[$this->getName()];
        $filesData = isset($files[$this->getName()]) ? $files[$this->getName()] : [];

        foreach ($this->fields as $section) {
            foreach ($section as $field) {
                $this->bindField(
                    $field,
                    $field instanceof File ? $filesData : $data
                );
            }
        }

        $this->bound = true;
    }

    /**
     * @param Widget $field
     * @param array $arr
     */
    protected function bindField(Widget $field, $arr)
    {
        $name = $field->getName();

        if (isset($arr[$name])) {
            $this->rawValues->set($name, $arr[$name]);

            if ($field instanceof BindableWidgetInterface) {
                $field->bind($arr[$name]);
            }

            if (!$field instanceof DontMapOnObject && !$field->isReadonly()) {
                $this->object->{$name} = $field->valueFormToObject($arr[$name]);
            }
        } elseif ($field instanceof WhenNotSetInRequest && !$field->isReadonly()) {
            $value = $field->whenNotSetInRequest();
            $this->rawValues->set($name, $value);
            $this->object->{$name} = $value;
        }
    }

    /**
     * @return bool
     */
    public function isBound()
    {
        return $this->bound;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        if (!$this->bound) {
            return false;
        }

        $valid = true;
        foreach ($this->fields as $section) {
            foreach ($section as $field) {
                if (!$field->isValid($this->getRawValue($field->getName()))) {
                    $valid = false;
                }
            }
        }

        $this->setCsrfToken();

        return $valid;
    }

    public function iterate($start = null, $stop = null)
    {
        $out = [];

        $started = !$start;

        foreach ($this->fields as $section => $fields) {
            foreach ($fields as $field) {
                if (!$started && $field->getName() === $start) {
                    $started = true;
                }

                if ($started) {
                    $out[] = $field;
                }

                if ($stop && $field->getName() === $stop) {
                    return $out;
                }
            }
        }

        return $out;
    }

    /**
     * @return Widget[][]
     */
    public function all()
    {
        return $this->fields;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface|null $container
     * @return Form
     */
    public function setContainer($container = null)
    {
        $this->container = $container;

        return $this;
    }

    public function setOption($key, $value)
    {
        $this->options->set($key, $value);

        return $this;
    }
}
