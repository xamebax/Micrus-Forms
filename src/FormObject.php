<?php
namespace Avris\Micrus\Forms;

use Avris\Bag\BagHelper;

class FormObject
{
    /** @var object */
    protected $original;

    /**
     * @param object|null $object
     */
    public function __construct($object = null)
    {
        $this->original = $object ?: new \stdClass();
    }

    /**
     * @return object
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value)
    {
        if ($this->hasAccessor('set', $key)) {
            $this->callAccessor('set', $key, $value);

            return;
        }

        if (!BagHelper::isArray($value) || !$this->hasAccessor('add', $key) || !$this->hasAccessor('remove', $key)) {
            if ($this->hasGenericAccessor('set')) {
                $this->callGenericAccessor('set', $key, $value);

                return;
            }

            $this->original->$key = $value;

            return;
        }

        $current = BagHelper::toArray($this->__get($key));

        $staying = array_uintersect(
            BagHelper::toArray($current),
            BagHelper::toArray($value),
            function($a, $b) {
                return strcmp(is_object($a) ? spl_object_hash($a) : $a, is_object($b) ? spl_object_hash($b) : $b);
            }
        );

        foreach ($current as $currentValue) {
            if (!in_array($currentValue, $staying)) {
                $this->callAccessor('remove', $key, $currentValue);
            }
        }

        foreach ($value as $singleValue) {
            if (!in_array($singleValue, $staying)) {
                $this->callAccessor('add', $key, $singleValue);
            }
        }
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        if ($this->hasAccessor('get', $key)) {
            return $this->callAccessor('get', $key);
        }

        if ($this->hasAccessor('is', $key)) {
            return $this->callAccessor('is', $key);
        }

        if ($this->hasAccessor('has', $key)) {
            return $this->callAccessor('has', $key);
        }

        if ($this->hasGenericAccessor('get')) {
            return $this->callGenericAccessor('get', $key);
        }

        return isset($this->original->$key) ? $this->original->$key : null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        $class = get_class($this->original);
        if (substr($class, 0, 10) == 'App\Model\\') {
            $class = substr($class, 10);
        }

        return str_replace('\\', '_', $class);
    }

    /**
     * @param string $type
     * @param string $key
     * @return bool
     */
    protected function hasAccessor($type, $key)
    {
        return method_exists($this->original, $type . ucfirst($key));
    }

    /**
     * @param string $type
     * @param string $key
     * @param mixed|null $value
     * @return mixed
     */
    protected function callAccessor($type, $key, $value = null)
    {
        return call_user_func([$this->original, $type . ucfirst($key)], $value);
    }

    /**
     * @param string $type
     * @return bool
     */
    protected function hasGenericAccessor($type)
    {
        return method_exists($this->original, $type);
    }

    /**
     * @param string $type
     * @param string $key
     * @param mixed|null $value
     * @return mixed
     */
    protected function callGenericAccessor($type, $key, $value = null)
    {
        return call_user_func([$this->original, $type], $key, $value);
    }

    /**
     * @param object $object
     * @param string $key
     * @return mixed
     */
    public static function get($object, $key)
    {
        $formObject = new FormObject($object);

        return $formObject->{$key};
    }

    /**
     * @param object $object
     * @param string $key
     * @param mixed $value
     * @return FormObject
     */
    public static function set($object, $key, $value)
    {
        $formObject = new FormObject($object);
        $formObject->{$key} = $value;

        return $formObject;
    }
}
