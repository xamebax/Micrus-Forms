<?php
namespace Avris\Micrus\Forms;

use Avris\Micrus\Localizator\Locale\YamlLocaleSet;
use Avris\Micrus\Bootstrap\Module;

class FormsModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'assertLocaleSet' => [
                    'class' => YamlLocaleSet::class,
                    'params' => ['assert', __DIR__ . '/Assert/Translations', 'en'],
                    'tags' => ['localeSet'],
                ],
            ],
        ];
    }
}
