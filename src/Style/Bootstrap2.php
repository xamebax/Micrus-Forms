<?php
namespace Avris\Micrus\Forms\Style;

use Avris\Micrus\Forms\Widget\Widget;

class Bootstrap2 extends FormStyle
{
    public function getReplacements(Widget $widget)
    {
        return [
            '{wrapper_before}' => '<div class="row">',
            '{wrapper_after}' => '</div>',
            '{widget_before}' => '<div class="col-lg-10">',
            '{widget_after}' => '</div>',
            '{label_class}' => 'form-label col-lg-2',
            '{wrapper_class}' => 'form-group',
            '{widget_class}' => 'form-control',
        ];
    }
}
