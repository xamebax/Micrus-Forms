<?php
namespace Avris\Micrus\Forms\Style;

use Avris\Micrus\Forms\Widget\Widget;

class BootstrapHalf extends FormStyle
{
    public function getReplacements(Widget $widget)
    {
        return [
            '{wrapper_before}' => '<div class="row">',
            '{wrapper_after}' => '</div>',
            '{widget_before}' => '<div class="col-sm-6">',
            '{widget_after}' => '</div>',
            '{label_class}' => 'form-label col-sm-6',
            '{wrapper_class}' => 'form-group',
            '{widget_class}' => 'form-control',
        ];
    }
}
