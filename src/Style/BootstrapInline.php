<?php
namespace Avris\Micrus\Forms\Style;

use Avris\Micrus\Forms\Widget\Widget;

class BootstrapInline extends FormStyle
{
    public function getReplacements(Widget $widget)
    {
        return [
            '{wrapper_before}' => '<div>',
            '{wrapper_after}' => '</div>',
            '{widget_before}' => '',
            '{widget_after}' => '',
            '{label_class}' => 'form-label',
            '{wrapper_class}' => 'form-group',
            '{widget_class}' => 'form-control',
        ];
    }
}
