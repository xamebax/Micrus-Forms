<?php
namespace Avris\Micrus\Forms\Style;

use Avris\Micrus\Forms\Widget\Widget;

class BootstrapInlineNoLabel extends FormStyle
{
    public function getReplacements(Widget $widget)
    {
        return [
            '<label for="{id}" class="{label_class}">{label}</label>' => '',
            '{extra}' => $widget->getOption('placeholder')
                ? ''
                : 'placeholder="' . htmlentities($widget->getOption('label') ?: ucfirst($widget->getName())) . '"',
            '{wrapper_before}' => '<div>',
            '{wrapper_after}' => '</div>',
            '{widget_before}' => '',
            '{widget_after}' => '',
            '{label_class}' => 'form-label',
            '{wrapper_class}' => 'form-group',
            '{widget_class}' => 'form-control',
        ];
    }
}
