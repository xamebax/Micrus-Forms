<?php
namespace Avris\Micrus\Forms\Style;

use Avris\Micrus\Forms\Widget\Widget;

class BootstrapMini extends FormStyle
{
    public function getReplacements(Widget $widget)
    {
        return [
            '{wrapper_before}' => '<div class="row"><div class="col-lg-12">',
            '{wrapper_after}' => '</div></div>',
            '{widget_before}' => '',
            '{widget_after}' => '',
            '{label_class}' => 'form-label',
            '{wrapper_class}' => 'form-group',
            '{widget_class}' => 'form-control  input-sm',
        ];
    }
}
