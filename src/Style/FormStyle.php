<?php
namespace Avris\Micrus\Forms\Style;

use Avris\Micrus\Forms\Widget\Widget;

abstract class FormStyle
{
    abstract public function getReplacements(Widget $widget);
}
