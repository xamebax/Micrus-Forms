<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

interface BindableWidgetInterface
{
    public function bind($data);
}
