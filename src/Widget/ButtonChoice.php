<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class ButtonChoice extends Choice
{
    protected function getTemplate($widgetValue = null)
    {
        $multiple = (bool)$this->options->get('multiple');
        if ($multiple && !is_array($widgetValue)) {
            $widgetValue = [];
        }
        $type = $multiple ? 'checkbox' : 'radio';
        $class = $this->form->getOptions('class') ?: 'btn-default btn-sm';

        $out = '<div class="btn-group btn-group-justified" data-toggle="buttons">';
        foreach ($this->options->get('choices') as $key => $val) {
            $active = $multiple ? (in_array($key, $widgetValue)) : ($key == $widgetValue);
            $out .= sprintf(
                '<label class="btn %s %s"><input type="%s" name="{name}%s" value="%s" {attributes} %s>%s</label>',
                $class,
                $active ? 'active' : '',
                $type,
                $multiple ? '['.$key.']' : '',
                $key,
                $active ? 'checked="checked"' : '',
                $val
            );
        }
        $out .= '</div>';

        return $out;
    }
}
