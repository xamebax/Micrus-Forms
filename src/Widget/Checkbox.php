<?php
namespace Avris\Micrus\Forms\Widget;

class Checkbox extends Widget implements WhenNotSetInRequest
{
    protected function getTemplate($widgetValue = null)
    {
        return sprintf(
            '<div class="checkbox"><label>' .
            '<input type="checkbox" name="{name}" value="true" {asserts} {attributes} %s><span>{sublabel}</span>' .
            '</label></div>',
            $widgetValue ? 'checked="checked"' : ''
        );
    }

    public function whenNotSetInRequest()
    {
        return false;
    }
}
