<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Bag\BagHelper;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\FormObject;

class Choice extends Widget implements WhenNotSetInRequest
{
    /** @var array */
    protected $choices = [];

    /** @var array */
    protected $objects = [];

    public function __construct(Form $form, $name, array $options = [], $asserts = [])
    {
        if (!isset($options['choices'])) {
            $options['choices'] = [];
        }

        $this->form = $form;
        $this->choices = $this->getChoices($options);

        parent::__construct($form, $name, $options, $asserts);

        if ($this->options->get('add_empty')) {
            $this->objects = ['' => null] + $this->objects;
        }
    }

    protected function getTemplate($widgetValue = null)
    {
        $choices = $this->choices;
        if ($this->options->get('add_empty')) {
            $choices = ['' => ''] + $choices;
        }

        $multiple = (bool) $this->options->get('multiple');
        if ($multiple) {
            $widgetValue = $widgetValue === null ? [] : BagHelper::toArray($widgetValue);
        }

        return $this->options->get('expanded')
            ? $this->getExpandedTemplate($choices, $multiple, $widgetValue)
            : $this->getNonExpandedTemplate($choices, $multiple, $widgetValue);
    }

    protected function getChoices($options)
    {
        if (isset($options['objects'])) {
            $choices = [];
            foreach ($options['objects'] as $object) {
                $id = FormObject::get($object, 'id');
                $this->objects[$id] = $object;
                $choices[$id] = (string) $object;
            }

            return $choices;
        }

        if (isset($options['model'])) {
            $choices = [];
            foreach ($this->fetchService('orm')->findAll($options['model']) as $object) {
                $choices[FormObject::get($object, 'id')] = (string) $object;
            }

            return $choices;
        }

        if (isset($options['choiceTranslation'])) {
            $prefix = $options['choiceTranslation'];

            return array_map(function ($value) use ($prefix) {
                return l($prefix . $value);
            }, $options['choices']);
        }

        return $options['choices'];
    }

    protected function getNonExpandedTemplate($choices, $multiple, $widgetValue)
    {
        $out = sprintf(
            '<select id="{id}" name="{name}%s" class="{widget_class}" %s {asserts} {attributes}>',
            $multiple ? '[]' : '',
            $multiple ? 'multiple="multiple"' : ''
        );

        foreach ($choices as $key => $value) {
            $kv = $this->options->has('objects') ? $this->objects[$key] : $key;
            $selected = $multiple ? in_array($kv, $widgetValue) : $kv == $widgetValue;
            $out .= sprintf(
                '<option value="%s" %s>%s</option>',
                htmlentities($key),
                $selected ? 'selected' : '',
                htmlentities($value)
            );
        }
        $out .= '</select>';

        return $out;
    }

    protected function getExpandedTemplate($choices, $multiple, $widgetValue)
    {
        $out = '';
        foreach ($choices as $key => $value) {
            $kv = $this->options->has('objects') ? $this->objects[$key] : $key;
            $out .= $multiple
                ? sprintf(
                    '<div class="checkbox"><label>' .
                    '<input type="checkbox" name="{name}[]" value="%s" {attributes} %s><span>%s</span>' .
                    '</label></div>',
                    htmlentities($key),
                    in_array($kv, $widgetValue) ? 'checked="checked"' : '',
                    htmlentities($value)
                ) : sprintf(
                    '<div class="radio"><label>' .
                    '<input type="radio" name="{name}" value="%s" {attributes} %s><span>%s</span>' .
                    '</label></div>',
                    htmlentities($key),
                    $kv == $widgetValue ? 'checked="checked"' : '',
                    htmlentities($value)
                );
        }
        return $out;
    }

    protected function getDefaultAssert()
    {
        return new Assert\Choice(
            $this->choices,
            (bool) $this->options->get('multiple')
        );
    }

    public function valueFormToObject($value)
    {
        if ($model = $this->options->get('model')) {
            if (!$value) {
                return $this->options->get('multiple') ? [] : null;
            }
            if ($this->options->get('multiple')) {
                return $this->fetchService('orm')->findBy($model, 'id', $value);
            }

            return $this->fetchService('orm')->find($model, $value);
        }

        if ($this->options->has('objects')) {
            if (!$value) {
                return $this->options->get('multiple') ? [] : null;
            }

            if ($this->options->get('multiple')) {
                $objects = [];
                foreach ($value as $item) {
                    if (isset($this->objects[$item])) {
                        $objects[$item] = $this->objects[$item];
                    }
                }

                return $objects;
            }

            return isset($this->objects[$value]) ? $this->objects[$value] : null;
        }

        return $value;
    }

    public function valueObjectToForm($value)
    {
        if ($this->options->get('model')) {
            if (!$value) {
                return null;
            }
            if ($this->options->get('multiple')) {
                $ids = [];
                foreach ($value as $el) {
                    $ids[] = FormObject::get($el, 'id');
                }

                return $ids;
            }

            return FormObject::get($value, 'id');
        }

        return $value;
    }

    public function whenNotSetInRequest()
    {
        return $this->options->get('multiple') ? [] : null;
    }
}
