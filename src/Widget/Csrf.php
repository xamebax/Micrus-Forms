<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class Csrf extends Hidden implements DontMapOnObject
{
    protected function getDefaultAssert()
    {
        return new Assert\Csrf();
    }

    public function getValue()
    {
        return $this->form->getRawValue('_csrf');
    }
}
