<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class Date extends Widget
{
    protected $template = '<input id="{id}" name="{name}" type="date"
        placeholder="yyyy-mm-dd" value="{value}" class="{widget_class}" {asserts} {attributes} {extra}/>';

    protected function getDefaultAssert()
    {
        return new Assert\Date();
    }

    public function valueFormToObject($value)
    {
        return $value ?: null;
    }

    public function valueObjectToForm($value)
    {
        return $value instanceof \DateTime ?
            $value->format('Y-m-d') :
            preg_match('#\d{4}-\d{2}-\d{2}#', $value, $matches) ? $matches[0] : null;
    }
}
