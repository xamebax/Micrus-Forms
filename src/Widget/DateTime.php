<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class DateTime extends Widget
{
    protected function getTemplate($widgetValue = null)
    {
        return sprintf(
            '<input id="{id}" name="{name}" type="%s" '.
            'placeholder="yyyy-mm-dd --:--" value="{value}" '.
            'class="{widget_class}" {asserts} {attributes} {extra}/>',
            $this->getOption('text') ? 'text' : 'datetime-local'
        );
    }

    protected function getDefaultAssert()
    {
        return new Assert\DateTime();
    }

    public function valueFormToObject($value)
    {
        return $value ? new \DateTime($value) : null;
    }

    public function valueObjectToForm($value)
    {
        if (!$value) {
            return null;
        }
        return $value instanceof \DateTime ?
            $value->format('Y-m-d\TH:i:s') :
            str_replace(' ', 'T', $value);
    }
}
