<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class Display extends Widget
{
    protected $template = '{value}';

    protected function getTemplate($widgetValue = null)
    {
        $template = $this->options->get('template') ?: $this->template;

        if (is_callable($template)) {
            return $template($widgetValue);
        }

        return $template;
    }
}
