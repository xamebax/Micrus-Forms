<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class Email extends Widget
{
    protected $template = '<input id="{id}" name="{name}" type="email" value="{value}"
        class="{widget_class}" {asserts} {attributes} {extra}/>';

    protected function getDefaultAssert()
    {
        return new Assert\Email();
    }

    public function valueFormToObject($value)
    {
        return $value ? strtolower(trim($value)) : null;
    }
}
