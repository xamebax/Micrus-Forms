<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class File extends Widget
{
    protected $template = '<input id="{id}" name="{name}" type="file"
        class="{widget_class}" {asserts} {attributes} {extra}/>';

    protected function getDefaultAssert()
    {
        return new Assert\File\File();
    }
}
