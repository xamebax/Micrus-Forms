<?php
namespace Avris\Micrus\Forms\Widget;

class Hidden extends Widget
{
    protected $wrapperTemplate = '{}{errors}';
    protected $template = '<input id="{id}" name="{name}" type="hidden"
        value="{value}" {asserts} {attributes} {extra}/>';
}
