<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;

class IconChoice extends ButtonChoice
{
    public function __construct(Form $form, $name, array $options, array $asserts)
    {
        $newChoices = [];
        foreach ($options['choices'] as $key => $value) {
            $newChoices[$key] = '<span class="'.$value.'"></span>';
        }
        $options['choices'] = $newChoices;

        parent::__construct($form, $name, $options, $asserts);
    }
}
