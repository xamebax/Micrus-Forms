<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class Integer extends Widget
{
    protected $template = '<input id="{id}" name="{name}" type="number" value="{value}"
        class="{widget_class}" {asserts} {attributes} {extra}/>';

    protected function getDefaultAssert()
    {
        return new Assert\Integer();
    }
}
