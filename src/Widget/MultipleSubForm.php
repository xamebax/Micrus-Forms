<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Bag\BagHelper;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Style\BootstrapInlineNoLabel;

class MultipleSubForm extends Widget implements BindableWidgetInterface, WhenNotSetInRequest
{
    /** @var Form[] */
    protected $forms = [];

    /** @var Form */
    protected $addForm;

    public function __construct(Form $form, $name, array $options = [], $asserts = [])
    {
        parent::__construct($form, $name, $options, $asserts);

        $this->forms = $this->buildForms($this->getValue());
        $this->addForm = $this->buildAddForm();
    }

    /**
     * @param array $elements
     * @return Form[]
     */
    protected function buildForms($elements)
    {
        $formClass = $this->getOption('form');
        $container = $this->getOption('container');

        $forms = [];

        foreach (BagHelper::toArray($elements) as $i => $element) {
            /** @var Form $form */
            $form = new $formClass($element, $container, [
                'csrf' => false,
                'name' => sprintf('%s[%s][%s]', $this->form->getName(), $this->name, $i),
            ]);
            $forms[$i] = $form;
        }

        return $forms;
    }

    /**
     * @param string $i
     * @return Form
     */
    protected function buildAddForm($i = '%i%')
    {
        $add = $this->getOption('add');

        if (!$add) {
            return null;
        }

        if ($add instanceof Form) {
            return $add;
        }

        $formClass = is_string($add) ? $add : $this->getOption('form');
        $container = $this->getOption('container');
        $model = $this->getOption('model');

        /** @var Form $form */
        $form = new $formClass(new $model, $container, [
            'csrf' => false,
            'name' => sprintf('%s[%s][%s]', $this->form->getName(), $this->name, $i),
        ]);

        return $form;
    }

    public function getTemplate($widgetValue = null)
    {
        $removeCondition = $this->getOption('remove');
        $removeButton = $removeCondition
            ? sprintf(
                '<td class="form-multiple-action"><button type="button" class="form-multiple-remove %s" %s>%s</button></td>',
                $this->getOption('btnRemoveClass') ?: 'btn btn-danger',
                $this->getOption('btnRemoveAttr'),
                $this->getOption('btnRemoveText') ?: '-'
            ) : '';

        $style = $this->getOption('lineStyle') ?: new BootstrapInlineNoLabel;

        $out = '<table class="table form-multiple">';

        $labelsForm = count($this->forms) ? reset($this->forms) : $this->addForm;
        if ($labelsForm) {
            $out .= '<tr>';
            foreach ($labelsForm->all() as $section) {
                /** @var Widget $widget */
                foreach ($section as $widget) {
                    $out .= '<th>' . $widget->getLabel() . '</th>';
                }
            }
            if ($removeButton) {
                $out .= '<th class="form-multiple-action"></th>';
            }
            $out .= '</tr>';
        }

        foreach ($this->forms as $i => $form) {
            $out .= '<tr data-index="' . $i . '">'
                . $form->render($style, '<td>', '</td>')
                . (($removeCondition === true || (is_callable($removeCondition) && $removeCondition($form->getObject())))
                    ? $removeButton : '<td></td>')
                . '</tr>';
        }

        if ($this->addForm) {
            $columnsCount = array_reduce($this->addForm->all(), function ($carry, array $item) {
                return $carry + count($item);
            });

            if (!$removeButton) {
                $columnsCount--;
            }

            $out .= sprintf('<tr><td colspan="%s"></td>', $columnsCount)
                . '<td class="form-multiple-action">'
                . sprintf(
                    '<button type="button" class="form-multiple-add %s" %s>%s</button>',
                    $this->getOption('btnAddClass') ?: 'btn btn-success',
                    $this->getOption('btnAddAttr'),
                    $this->getOption('btnAddText') ?: '+'
                )
                . '<script type="text/template" class="form-multiple-add-template">'
                . '<tr data-index="%i%">'
                . $this->addForm->render($style, '<td>', '</td>')
                . $removeButton
                . '</tr>'
                . '</script>'
                . '</td></tr>';
        }

        return $out . '</table>';
    }

    public function isValid($value)
    {
        $valid = parent::isValid($value);

        foreach ($this->forms as $i => $form) {
            if (isset($value[$i])) {
                $valid = $valid && $form->isValid();
            }
        }

        return $valid;
    }

    public function bind($data)
    {
        foreach($data as $i => $element) {
            $form = isset($this->forms[$i])
                ? $this->forms[$i]
                : $this->buildAddForm($i);

            $form->bind([$form->getName() => $data[$i]]);

            $this->forms[$i] = $form;
        }
    }

    public function valueFormToObject($value)
    {
        $out = [];

        foreach ($this->forms as $i => $form) {
            if (isset($value[$i])) {
                $out[$i] = $form->getObject();
            }
        }

        return $out;
    }

    public function whenNotSetInRequest()
    {
        return [];
    }
}
