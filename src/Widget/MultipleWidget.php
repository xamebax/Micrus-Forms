<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Bag\BagHelper;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Style\BootstrapInlineNoLabel;

class MultipleWidget extends Widget implements BindableWidgetInterface, WhenNotSetInRequest
{
    /** @var Widget[] */
    protected $widgets = [];

    /** @var Widget */
    protected $addWidget;

    public function __construct(Form $form, $name, array $options = [], $asserts = [])
    {
        parent::__construct($form, $name, $options, $asserts);

        $this->widgets = $this->buildWidgets($this->getValue());
        $this->addWidget = $this->buildAddWidget();
    }

    /**
     * @param array $elements
     * @return Widget[]
     */
    protected function buildWidgets($elements)
    {
        $widgetClass = $this->getOption('widget');
        $widgetOptions = $this->getOption('widgetOptions', []);
        $widgetAsserts = $this->getOption('widgetAsserts', []);

        $widgets = [];

        foreach (BagHelper::toArray($elements) as $i => $element) {
            $widgetOptions['id'] = sprintf('%s_%s_%s', $this->form->getName(), $this->name, $i);
            $widgetOptions['name'] = sprintf('%s[%s][%s]', $this->form->getName(), $this->name, $i);

            /** @var Widget $widget */
            $widget = new $widgetClass(
                $this->form,
                $this->name,
                $widgetOptions,
                $widgetAsserts
            );
            $widget->forcedValue = $element;
            $widgets[$i] = $widget;
        }

        return $widgets;
    }

    /**
     * @param string $i
     * @return Form
     */
    protected function buildAddWidget($i = '%i%')
    {
        $add = $this->getOption('add');

        if (!$add) {
            return null;
        }

        if ($add instanceof Widget) {
            return $add;
        }

        $widgetClass = $this->getOption('widget');
        $widgetOptions = $this->getOption('widgetOptions', []);
        $widgetAsserts = $this->getOption('widgetAsserts', []);

        $widgetOptions['id'] = sprintf('%s_%s_%s', $this->form->getName(), $this->name, $i);
        $widgetOptions['name'] = sprintf('%s[%s][%s]', $this->form->getName(), $this->name, $i);

        /** @var Widget $widget */
        $widget = new $widgetClass(
            $this->form,
            $this->name,
            $widgetOptions,
            $widgetAsserts
        );
        $widget->forcedValue = '';
        $widgets[$i] = $widget;

        return $widget;
    }

    public function getTemplate($widgetValue = null)
    {
        $removeCondition = $this->getOption('remove');
        $removeButton = $removeCondition
            ? sprintf(
                '<td class="form-multiple-action"><button type="button" class="form-multiple-remove %s" %s>%s</button></td>',
                $this->getOption('btnRemoveClass') ?: 'btn btn-danger',
                $this->getOption('btnRemoveAttr'),
                $this->getOption('btnRemoveText') ?: '-'
            ) : '';

        $style = $this->getOption('lineStyle') ?: new BootstrapInlineNoLabel;

        $out = '<table class="table form-multiple">';

        foreach ($this->widgets as $i => $widget) {
            $out .= '<tr data-index="' . $i . '">'
                . '<td>' . $widget->render($style) . '</td>'
                . (($removeCondition === true || (is_callable($removeCondition) && $removeCondition($widget->getValue())))
                    ? $removeButton : '<td></td>')
                . '</tr>';
        }

        if ($this->addWidget) {
            $out .= '<tr>'
                . ($removeButton ? '<td></td>' : '')
                . '<td class="form-multiple-action">'
                . sprintf(
                    '<button type="button" class="form-multiple-add %s" %s>%s</button>',
                    $this->getOption('btnAddClass') ?: 'btn btn-success',
                    $this->getOption('btnAddAttr'),
                    $this->getOption('btnAddText') ?: '+'
                )
                . '<script type="text/template" class="form-multiple-add-template">'
                . '<tr data-index="%i%">'
                . '<td>' . $this->addWidget->render($style) . '</td>'
                . $removeButton
                . '</tr>'
                . '</script>'
                . '</td></tr>';
        }

        return $out . '</table>';
    }

    public function isValid($value)
    {
        $valid = parent::isValid($value);

        foreach ($this->widgets as $i => $widget) {
            if (isset($value[$i])) {
                $valid = $valid && $widget->isValid($value[$i]);
            }
        }

        return $valid;
    }

    public function bind($data)
    {
        foreach($data as $i => $element) {
            $widget = isset($this->widgets[$i])
                ? $this->widgets[$i]
                : $this->buildAddWidget($i);

            $widget->forcedValue = $data[$i];

            $this->widgets[$i] = $widget;
        }
    }

    public function valueFormToObject($value)
    {
        $out = [];

        foreach ($this->widgets as $i => $widget) {
            if (isset($value[$i])) {
                $out[$i] = $widget->valueFormToObject($value[$i]);
            }
        }

        return $out;
    }

    public function valueObjectToForm($value)
    {
        $out = [];

        foreach ($this->widgets as $i => $widget) {
            if (isset($value[$i])) {
                $out[$i] = $widget->valueObjectToForm($value[$i]);
            }
        }

        return $out;
    }

    public function whenNotSetInRequest()
    {
        return [];
    }
}
