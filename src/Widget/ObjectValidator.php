<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Exception\InvalidArgumentException;

class ObjectValidator extends Widget
{
    protected $wrapperTemplate = '<div class="{wrapper_class} {has_error} form-general-errors">
        {wrapper_before}<div class="{label_class}"></div>{widget_before}{errors}{widget_after}{wrapper_after}</div>';
    protected $template = '';

    public function __toString()
    {
        $replacements = $this->buildReplacements();
        return str_replace(array_keys($replacements), array_values($replacements), $this->wrapperTemplate);
    }

    public function getValue()
    {
        return null;
    }

    protected function getDefaultAssert()
    {
        $callback = $this->options->get('callback') ?: [$this->form, $this->getName()];

        if (!is_callable($callback)) {
            throw new InvalidArgumentException(sprintf(
                'ObjectValidator "%s" doesn\'t have a valid callback specified',
                $this->getName()
            ));
        }

        return new Assert\ObjectValidator($callback, $this->form->getObject(false));
    }
}
