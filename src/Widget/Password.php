<?php
namespace Avris\Micrus\Forms\Widget;

class Password extends Widget
{
    protected $template = '<input id="{id}" name="{name}" type="password"
        class="{widget_class}" {asserts} {attributes} {extra}/>';
}
