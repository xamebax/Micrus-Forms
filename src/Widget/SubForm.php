<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Bag\BagHelper;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Style\Bootstrap;

class SubForm extends Widget implements BindableWidgetInterface
{
    /** @var Form */
    protected $form;

    public function __construct(Form $form, $name, array $options = [], $asserts = [])
    {
        parent::__construct($form, $name, $options, $asserts);

        $this->form = $this->buildForm($this->getValue());
    }

    /**
     * @param mixed $value
     * @return Form
     */
    protected function buildForm($value)
    {
        $formClass = $this->getOption('form');
        $container = $this->getOption('container');

        return  new $formClass($value, $container, [
            'csrf' => false,
            'name' => sprintf('%s[%s]', $this->form->getName(), $this->name),
        ]);
    }

    public function getTemplate($widgetValue = null)
    {
        $style = $this->getOption('style') ?: new Bootstrap;

        return $this->form->render($style);
    }

    public function isValid($value)
    {
        return $this->form->isValid();
    }

    public function bind($data)
    {
        $this->form->bind([$this->form->getName() => $data]);
    }

    public function valueFormToObject($value)
    {
        return $this->form->getObject();
    }
}
