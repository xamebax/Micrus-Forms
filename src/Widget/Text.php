<?php
namespace Avris\Micrus\Forms\Widget;

class Text extends Widget
{
    protected $template = '<input id="{id}" name="{name}" type="text" value="{value}"
        class="{widget_class}" {asserts} {attributes} {extra}/>';
}
