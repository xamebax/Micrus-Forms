<?php
namespace Avris\Micrus\Forms\Widget;

class TextAddon extends Text
{
    public function getTemplate($widgetValue = null)
    {
        return sprintf(
            '<div class="input-group">' .
                '%s' .
                '<input id="{id}" name="{name}" type="text" value="{value}" ' .
                'class="{widget_class}" {asserts} {attributes} {extra}/>' .
                '%s' .
            '</div>',
            $this->renderAddon($this->options->get('before')),
            $this->renderAddon($this->options->get('after'))
        );
    }

    protected function renderAddon($text)
    {
        return $text ? '<div class="input-group-addon">' . $text . '</div>' : null;
    }
}
