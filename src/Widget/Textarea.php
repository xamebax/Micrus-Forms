<?php
namespace Avris\Micrus\Forms\Widget;

class Textarea extends Widget
{
    protected $template = '<textarea id="{id}" name="{name}"
        class="{widget_class}" {asserts} {attributes} {extra}>{value}</textarea>';
}
