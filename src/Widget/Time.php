<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;

class Time extends Widget
{
    protected $template = '<input id="{id}" name="{name}" type="time" placeholder="--:--"
        value="{value}" class="{widget_class}" {asserts} {attributes} {extra}/>';

    protected function getDefaultAssert()
    {
        return new Assert\Time();
    }

    public function valueFormToObject($value)
    {
        return $value ?: null;
    }
}
