<?php
namespace Avris\Micrus\Forms\Widget;

interface WhenNotSetInRequest
{
    public function whenNotSetInRequest();
}
