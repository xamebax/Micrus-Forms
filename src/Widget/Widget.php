<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;
use Avris\Bag\Bag;
use Avris\Micrus\Forms\Style\FormStyle;

abstract class Widget
{
    /** @var Form */
    protected $form;

    /** @var string */
    protected $name;

    /** @var Bag */
    protected $options;

    /** @var string */
    protected $wrapperTemplate = '<div class="{wrapper_class} {has_error} {required}">{wrapper_before}
        <label for="{id}" class="{label_class}">{label}</label>{widget_before}{}{errors}{helper}{widget_after}
        {wrapper_after}</div>';

    /** @var string */
    protected $template = '';

    /** @var Assert\Assert[] */
    protected $asserts;

    /** @var string[] */
    protected $messages = [];

    /** @var mixed|false */
    protected $forcedValue = false;

    /**
     * @param Form $form
     * @param string $name
     * @param array $options
     * @param Assert\Assert[] $asserts
     */
    public function __construct(Form $form, $name, array $options = [], $asserts = [])
    {
        $this->form = $form;
        $this->name = $name;
        $this->options = new Bag($options);
        $this->asserts = $this->manageAsserts($asserts);
    }

    public function __toString()
    {
        return $this->render();
    }

    public function render(FormStyle $style = null)
    {
        $replacements = $this->buildReplacements($style);

        if ($replacements['{}'] === false) {
            return '';
        }

        return str_replace(
            array_keys($replacements),
            array_values($replacements),
            str_replace(
                array_keys($replacements),
                array_values($replacements),
                $this->getWrapperTemplate()
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->forcedValue === false
            ? $this->form->getObject(false)->{$this->name}
            : $this->forcedValue;
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function valueFormToObject($value)
    {
        return $value;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function valueObjectToForm($value)
    {
        return $value;
    }

    /**
     * @param string|array $value
     * @return bool
     */
    public function isValid($value)
    {
        $messages = [];
        foreach ($this->asserts as $assert) {
            if (Assert\Assert::isEmpty($value) && !$assert instanceof Assert\IsRequired) {
                continue;
            }
            $message = $assert->validate($value);
            if ($message === true) {
                continue;
            }

            $messages[] = $message ?: l('validator.' . $assert->getName(), $assert->getReplacements());
        }
        $this->messages = $messages;

        return count($messages) === 0;
    }

    /**
     * @param FormStyle $style
     * @return array
     */
    protected function buildReplacements(FormStyle $style = null)
    {
        $value = $this->valueObjectToForm($this->getValue());
        $helper = $this->options->get('helper');

        $replacements = array_merge([
            '{}' => $this->getTemplate($value),
            '<label for="{id}" class="{label_class}">{label}</label>'
                => '<label for="{id}" class="{label_class}">{label}</label>',
            '{wrapper_before}' => '',
            '{wrapper_after}' => '',
            '{widget_before}' => '',
            '{widget_after}' => '',
            '{label_class}' => '',
            '{wrapper_class}' => '',
            '{widget_class}' => '',
            '{extra}' => '',
            '{has_error}' => count($this->messages) ? 'has-error' : '',
            '{id}' => $this->getOption('id', strtr($this->form->getName(), ['[' => '_', ']' => '']) . '_' . $this->name),
            '{name}' => $this->getOption('name', $this->form->getName() . '[' . $this->name . ']'),
            '{label}' => $this->getLabel(),
            '{sublabel}' => $this->options->get('sublabel'),
            '{value}' => is_scalar($value) ? htmlentities($value) : '',
            '{errors}' => $this->getErrorsHtml(),
            '{asserts}' => $this->getAssertsHtmlAttributes(),
            '{attributes}' => $this->getHtmlAttributes(),
            '{required}' => $this->isRequired() ? 'required' : '',
            '{helper}' => $helper ? '<div class="form-helper"><small>{helper_value}</small></div>' : '',
            '{helper_value}' => $helper,
        ], $style ? $style->getReplacements($this) : []);

        $replacements['{widget_class}'] = $this->mergeWidgetClass($replacements['{widget_class}']);

        return $replacements;
    }

    /**
     * @return string
     */
    public function getErrorsHtml()
    {
        if (!count($this->messages)) {
            return '';
        }

        $out = '<ul class="form-errors">';
        foreach ($this->messages as $message) {
            $out .= '<li>' . l($message) . '</li>';
        }
        return $out . '</ul>';
    }

    /**
     * @return string
     */
    protected function getAssertsHtmlAttributes()
    {
        $attributes = [];
        foreach ($this->asserts as $assert) {
            $attributes = array_merge($attributes, $assert->getHtmlAttributes());
        }

        return implode(' ', array_unique($attributes));
    }

    /**
     * @return string
     */
    protected function getHtmlAttributes()
    {
        $attr = $this->options->get('attr');
        if ($placeholder = $this->options->get('placeholder')) {
            $attr['placeholder'] = $placeholder;
        }
        if (!$attr || !is_array($attr)) {
            return '';
        }

        $attrStrings = [];
        foreach ($attr as $name => $value) {
            if (in_array($name, ['id', 'name', 'type', 'value', 'class'])) {
                continue;
            }
            $attrStrings[] = $value === true ? $name : $name.'="' . htmlentities($value) . '"';
        }

        return implode(' ', $attrStrings);
    }

    /**
     * @param string $currentClasses
     * @return string
     */
    protected function mergeWidgetClass($currentClasses)
    {
        $attr = $this->options->get('attr');
        if ($class = $this->options->get('class')) {
            $attr['class'] = $class;
        }

        if ($attr && isset($attr['class'])) {
            $classes = explode(' ', $currentClasses);
            $customClasses = $attr['class'];
            if (!is_array($customClasses)) {
                $customClasses = explode(' ', $customClasses);
            }
            $classes = array_unique(array_merge($classes, $customClasses));

            return implode(' ', $classes);
        }

        return $currentClasses;
    }

    /**
     * @return Assert\Assert|null
     */
    protected function getDefaultAssert()
    {
        return null;
    }

    protected function manageAsserts($asserts)
    {
        $defaultAssert = $this->getDefaultAssert();
        if (!$defaultAssert) {
            return $asserts;
        }

        $defaultAssertClass = get_class($defaultAssert);
        $useDefault = true;
        foreach ($asserts as $assert) {
            if ($assert instanceof $defaultAssertClass) {
                $useDefault = false;
            }
        }
        if ($useDefault) {
            $asserts[] = $defaultAssert;
        }

        return $asserts;
    }

    /**
     * @return string
     */
    protected function getWrapperTemplate()
    {
        return $this->wrapperTemplate;
    }

    /**
     * @param string $widgetValue
     * @return string
     */
    protected function getTemplate($widgetValue = null)
    {
        return $this->template;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        foreach ($this->asserts as $assert) {
            if ($assert instanceof Assert\IsRequired) {
                return true;
            }
        }

        return false;
    }

    protected function fetchService($name)
    {
        $container = $this->form->getContainer();

        if ($container && $container->has($name)) {
            return $container->get($name);
        }

        if ($this->form->getOptions()->offsetExists($name)) {
            return $this->form->getOptions($name);
        }

        throw new InvalidArgumentException(sprintf(
            'Widget %s requires service %s to be injected into the form',
            get_class($this),
            $name
        ));
    }

    public function getOption($name, $default = null)
    {
        return $this->options->get($name, $default);
    }

    public function setOption($name, $value)
    {
        $this->options->set($name, $value);

        return $this;
    }

    public function isReadonly()
    {
        return (bool) $this->getOption('readonly');
    }

    public function getLabel()
    {
        return isset($this->options['label'])
            ? $this->options['label']
            : l(sprintf('entity.%s.fields.%s', $this->form->getModelName(), $this->getName()));
    }
}
