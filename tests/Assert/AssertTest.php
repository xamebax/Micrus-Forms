<?php
namespace Avris\Micrus\Forms\Assert;

abstract class AssertTest extends \PHPUnit_Framework_TestCase
{
    /** @var Assert */
    protected $assert;

    public function assertValidFor($value)
    {
        $this->assertTrue($this->assert->validate($value));
    }

    public function assertInvalidFor($value)
    {
        $this->assertFalse($this->assert->validate($value));
    }

    public function assertValidation($expected, $value)
    {
        if ($expected) {
            $this->assertValidFor($value);
        } else {
            $this->assertInvalidFor($value);
        }
    }
}