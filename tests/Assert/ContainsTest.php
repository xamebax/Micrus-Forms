<?php
namespace Avris\Micrus\Forms\Assert;

class ContainsTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Contains('test');
        $this->assertValidFor('protest');
        $this->assertValidFor('testowy');
        $this->assertValidFor('atesty');
        $this->assertInvalidFor('');
        $this->assertInvalidFor(0);
        $this->assertInvalidFor('1,2');
        $this->assertInvalidFor('foo');

        $this->assertEquals(['%value%' => 'test'], $this->assert->getReplacements());
    }
}
