<?php
namespace Avris\Micrus\Forms\Assert;

use Avris\Micrus\Model\User\MemoryUser;
use Avris\Micrus\Tool\Security\CryptInterface;

class CorrectPasswordTest extends AssertTest
{
    public function testAssert()
    {
        $crypt = $this->getMockBuilder(CryptInterface::class)->disableOriginalConstructor()->getMock();
        $crypt->expects($this->any())->method('validate')->willReturnCallback(
            function($password, $dbPassword) { return $password === $dbPassword; }
        );

        $this->assert = new CorrectPassword(
            function() {
                return new MemoryUser('foo', [
                    'authenticators' => [
                        'password' => 'bar',
                    ]
                ]);
            },
            $crypt
        );

        $this->assertInvalidFor('');
        $this->assertValidFor('bar');
        $this->assertInvalidFor('baz');

        $this->assert = new CorrectPassword(
            function() { return null; },
            $crypt
        );

        $this->assertInvalidFor('bar');
        $this->assertInvalidFor('baz');
    }
}