<?php
namespace Avris\Micrus\Forms\Assert;

class CsrfTest extends AssertTest
{
    public function testAssert()
    {
        if (!session_id()) {
            session_start();
        }
        $_SESSION['_csrf'] = 'foo_csrf';

        $this->assert = new Csrf();
        $this->assertValidFor('foo_csrf');
        $this->assertInvalidFor('ooo');
    }
}
