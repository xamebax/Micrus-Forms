<?php
namespace Avris\Micrus\Forms\Assert;

class DateTimeTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new DateTime();
        $this->assertInvalidFor('');
        $this->assertValidFor('2015-09-30 00:00');
        $this->assertValidFor('2015-09-30 00:00:00');
        $this->assertValidFor('2015-09-30T00:00:00');
        $this->assertInvalidFor('2015-09-30');
        $this->assertInvalidFor('2015-09-31');
        $this->assertInvalidFor('foo');

        $this->assertEquals(['pattern="^\d{4}-\d{2}-\d{2} \d{2}:\d{2}$"'], $this->assert->getHtmlAttributes());
    }
}