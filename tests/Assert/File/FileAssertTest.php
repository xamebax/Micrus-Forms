<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\AssertTest;

abstract class FileAssertTest extends AssertTest
{
    const CLS = '';

    const VALUE = 1000;

    public static $mockedImageSize;

    protected function getImagePath()
    {
        return __DIR__ . '/../../_help/favicon.ico';
    }

    /** @dataProvider imageSizeProvider */
    public function testAssert($given, $expected)
    {
        $className = static::CLS;
        $this->assert = new $className(static::VALUE);

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getTmpName')->willReturn($this->getImagePath());

        self::$mockedImageSize = $given;

        $this->assertValidation($expected, $uploadedFile);

        $this->assertEquals(['%value%' => static::VALUE], $this->assert->getReplacements());
    }
}

function getimagesize() {
    return FileAssertTest::$mockedImageSize;
}