<?php
namespace Avris\Micrus\Forms\Assert\File;

use Avris\Bag\Bag;
use Avris\Micrus\Bootstrap\Container;
use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Forms\Assert\AssertTest;

class ImageTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Image();

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getTmpName')->willReturn(__FILE__);
        $this->assertInvalidFor($uploadedFile);

        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();
        $uploadedFile->expects($this->once())->method('getTmpName')->willReturn(__DIR__ . '/../../_help/favicon.ico');
        $this->assertValidFor($uploadedFile);
    }
}