<?php
namespace Avris\Micrus\Forms\Assert\File;

class RatioHorizontalTest extends FileAssertTest
{
    const CLS = Ratio::class;

    const VALUE = Ratio::HORIZONTAL;

    public function imageSizeProvider()
    {
        return [
            [[800, 1000], false],
            [[800, 800], true],
            [[800, 600], true],
        ];
    }
}
