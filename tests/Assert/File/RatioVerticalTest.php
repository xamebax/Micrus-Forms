<?php
namespace Avris\Micrus\Forms\Assert\File;

class RatioVerticalTest extends FileAssertTest
{
    const CLS = Ratio::class;

    const VALUE = Ratio::VERTICAL;

    public function imageSizeProvider()
    {
        return [
            [[800, 1000], true],
            [[800, 800], true],
            [[800, 600], false],
        ];
    }
}
