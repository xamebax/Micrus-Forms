<?php
namespace Avris\Micrus\Forms\Assert;

class MaxTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new Max(-5);
        $this->assertInvalidFor('');
        $this->assertValidFor('-5');
        $this->assertValidFor('-6');
        $this->assertValidFor('-6.5');
        $this->assertInvalidFor(0);
        $this->assertInvalidFor(-4.5);

        $this->assertEquals(['max="-5"'], $this->assert->getHtmlAttributes());
        $this->assertEquals(['%value%' => -5], $this->assert->getReplacements());
    }
}