<?php
namespace Avris\Micrus\Forms\Assert;

class MinCountTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new MinCount(2);
        $this->assertInvalidFor('');
        $this->assertInvalidFor([]);
        $this->assertInvalidFor([1]);
        $this->assertValidFor([1, 2]);
        $this->assertValidFor([1, 2, 3]);
        $this->assertEquals(['%value%' => 2], $this->assert->getReplacements());
    }
}