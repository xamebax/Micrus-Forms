<?php
namespace Avris\Micrus\Forms\Assert;

class MinLengthTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new MinLength(5);
        $this->assertInvalidFor('');
        $this->assertValidFor('12345');
        $this->assertValidFor('abcde');
        $this->assertValidFor('abcdef');
        $this->assertInvalidFor('abc');

        $this->assertEquals(['minLength="5"'], $this->assert->getHtmlAttributes());
        $this->assertEquals(['%value%' => 5], $this->assert->getReplacements());
    }
}