<?php
namespace Avris\Micrus\Forms\Assert;

class NotBlankTest extends AssertTest
{
    public function testAssert()
    {
        $this->assert = new NotBlank();
        $this->assertValidFor('a');
        $this->assertValidFor('0');
        $this->assertValidFor(['a']);
        $this->assertInvalidFor('');
        $this->assertInvalidFor(null);
        $this->assertInvalidFor([]);

        $this->assertEquals(['required'], $this->assert->getHtmlAttributes());
    }
}