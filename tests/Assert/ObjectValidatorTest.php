<?php
namespace Avris\Micrus\Forms\Assert;

class ObjectValidatorTest extends AssertTest
{
    public function testAssert()
    {
        $obj = new \stdClass();
        $this->assert = new ObjectValidator([$this, 'valCallback'], $obj);

        $obj->field1 = 'foo';
        $obj->field2 = 'foo';
        $this->assertValidFor($obj);

        $obj->field2 = 'bar';
        $this->assertInvalidFor($obj);
    }

    public function valCallback($object)
    {
        return $object->field1 == $object->field2 ? true : false;
    }
}