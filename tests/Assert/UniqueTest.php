<?php
namespace Avris\Micrus\Forms\Assert;

use Avris\Micrus\Forms\FormObject;
use Avris\Micrus\Model\NoORM;

class UniqueTest extends AssertTest
{
    public function testAssert()
    {
        $orm = $this->getMockBuilder(NoORM::class)->disableOriginalConstructor()->getMock();
        $orm->expects($this->any())->method('findBy')->willReturnCallback(function($type, $field, $value) {
            if ($value == 'foo') {
                $object = new \stdClass();
                $object->id = 6;
                $object->data = 'foo';

                return [$object];
            }

            return [];
        });

        $object = new FormObject();
        $object->id = 5;
        $object->data = 'foo';

        $this->assert = new Unique($orm, $object, 'dummy', 'data');
        $this->assertInvalidFor($object->data);

        $object->data = 'bar';
        $this->assertValidFor($object->data);

        $object->id = 6;
        $object->data = 'foo';
        $this->assertValidFor($object->data);
    }
}
