<?php
namespace Avris\Micrus\Forms;

use Avris\Micrus\Model\NoORM;
use Avris\Micrus\Test\TestForm;

abstract class BaseTest extends \PHPUnit_Framework_TestCase
{
    /** @var TestForm */
    protected $form;

    protected function setUp()
    {
        $this->form = new TestForm(null, null, ['orm' => new NoORM()]);;
    }

    protected function assertException(callable $callable, $class, $message)
    {
        try {
            call_user_func($callable);
            $this->fail('Exception expected');
        } catch (\Exception $e) {
            $this->assertInstanceOf($class, $e);
            $this->assertEquals($message, $e->getMessage());
        }
    }

    protected function assertHtmlFile($file, $actual)
    {
        $this->assertEquals(
            file_get_contents(__DIR__ . '/_help/' . $file),
            $this->normalizeHtml($actual)
        );
    }

    protected function normalizeHtml($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->normalizeDocument();
        return $dom->saveXML($dom->documentElement);
    }
}