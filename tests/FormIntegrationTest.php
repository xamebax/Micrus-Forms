<?php
namespace Avris\Micrus\Forms;

use App\Model\Testie;
use Avris\Bag\Bag;
use Avris\Micrus\Bootstrap\Container;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Forms\Widget\Text;
use Avris\Micrus\Model\NoORM;
use Avris\Micrus\Test\TestForm;
use Avris\Micrus\Forms\Style\Bootstrap;

class FormIntegrationTest extends BaseTest
{
    public function testForm()
    {
        $_SESSION['_csrf'] = 'foo_csrf';

        $obj = new \stdClass;

        $orm = $this->getMockBuilder(NoORM::class)->disableOriginalConstructor()->getMock();
        $orm->expects($this->any())->method('findAll')->willReturn([
            new Testie(6, 'sześć'),
            new Testie(8, 'ośm'),
        ]);
        $orm->expects($this->any())->method('findBy')->willReturnCallback(function($model, $attr, $value) {
            if ($attr == 'id' && $value == ['6', '8']) {
                return new Bag([new Testie(6, 'sześć'), new Testie(8, 'ośm')]);
            } else if ($attr == 'id' && $value == ['8']) {
                return [new Testie(8, 'ośm')];
            }
            return [];
        });
        $orm->expects($this->any())->method('find')->willReturn(new Testie(8, 'ośm'));

        $container = new Container();
        $container->set('orm', $orm);

        $form = new TestForm($obj, $container, ['isAdmin' => false]);
        $this->assertSame($container, $form->getContainer());

        $this->assertTrue($form->has('text'));
        $this->assertFalse($form->has('nonexistent'));

        $this->assertEquals('stdClass', $form->getName());
        $this->assertNull($form->get('invisible'));
        $this->assertInstanceOf(Text::class, $form->get('text'));
        $this->assertInstanceOf(FormObject::class, $form->getObject(false));
        $this->assertSame($obj, $form->getObject());

        $this->assertFalse($form->isBound());
        $this->assertFalse($form->isValid());

        $this->assertException([$form, 'triggerInvalidWidget'], InvalidArgumentException::class, 'Widget of type Nonexistent not found');
        $this->assertException([$form, 'triggerDuplicateWidget'], InvalidArgumentException::class, 'Cannot redefine widget "text"');
        $this->assertException([$form, 'triggerInvalidObjectValidator'], InvalidArgumentException::class, 'ObjectValidator "foo" doesn\'t have a valid callback specified');

        try {
            $form->setContainer(new Container());
            $form->get('choiceModel')->valueFormToObject(['5']);
            $this->fail('Exception expected');
        } catch (InvalidArgumentException $e) {
            $this->assertEquals('Widget Avris\Micrus\Forms\Widget\Choice requires service orm to be injected into the form', $e->getMessage());
            $form->setContainer($container);
        }

        $form->setContainer(null);
        $form->setOption('orm', $orm);
        $object = $form->get('choiceModel')->valueFormToObject(['5']);
        $this->assertInstanceOf(Testie::class, $object);
        $form->setContainer($container);
        $form->setOption('orm', null);

        $this->assertHtmlFile(
            'integration/notBoundNoStyle.html',
            (string) $form
        );

        $this->assertHtmlFile(
            'integration/notBound.html',
            $form->render(Bootstrap::class)
        );

        $this->assertEquals((object)[], $obj);

        $form->bind(['nope']);
        $this->assertFalse($form->isBound());
        $this->assertFalse($form->isValid());

        $form->bind(['stdClass' => [
            '_csrf' => 'false',
            'int' => '5.5',
            'choiceModelM' => ['6', '8'],
        ]]);
        $this->assertTrue($form->isBound());
        $this->assertFalse($form->isValid());
        $this->assertEquals('foo_csrf', $form->getRawValue('_csrf'));

        $this->assertHtmlFile(
            'integration/boundWithErrors.html',
            $form->render(Bootstrap::class)
        );

        $this->assertEquals((object)[
            'agree' => false,
            'int' => '5.5',
            'choiceEmpty' => null,
            'choiceSN' => null,
            'choiceMN' => [],
            'choiceSE' => null,
            'choiceME' => [],
            'choiceModel' => null,
            'choiceModelM' => new Bag([
                new Testie(6, 'sześć'),
                new Testie(8, 'ośm'),
            ]),
            'buttons' => [],
            'icons' => null,
            'multiWidget' => [],
            'multiSubForm' => [],
        ], $obj);

        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->any())->method('isPost')->willReturn(true);
        $request->expects($this->any())->method('getFiles')->willReturn([]);
        $request->expects($this->any())->method('getData')->willReturn(['stdClass' => [
            '_csrf' => 'foo_csrf',
            'text' => 'foo',
            'requiredText' => 'bar',
            'email' => 'test@micrus.avris.it',
            'date' => '2016-02-01',
            'time' => '16:30',
            'datetime' => '2016-02-01 16:30',
            'int' => '5',
            'c3' => 'foobar',
            'choiceMN' => ['1', '3'],
            'choiceSE' => '2',
            'choiceModel' => '8',
            'choiceModelM' => ['8'],
            'buttons' => ['1', '3'],
            'icons' => 'ok',
            'multiWidget' => ['test@wp.pl', 'ok@gmail.com'],
            'multiSubForm' => [
                ['email' => 'test1@ok.pl', 'choice' => 0],
                ['email' => 'test2@ok.pl', 'choice' => 1],
            ],
            'subForm' => ['email' => 'test3@ok.pl', 'choice' => 1],
        ]]);

        $form->bindRequest($request);

        $this->assertTrue($form->isBound());
        $this->assertTrue($form->isValid());

        $this->assertHtmlFile(
            'integration/boundWithoutErrors.html',
            $form->render(Bootstrap::class)
        );

        $this->assertEquals((object)[
            'agree' => false,
            'int' => '5',
            'choiceEmpty' => null,
            'choiceSN' => null,
            'choiceMN' => ['1', '3'],
            'choiceSE' => '2',
            'choiceME' => [],
            'choiceModel' => new Testie(8, 'ośm'),
            'choiceModelM' => [new Testie(8, 'ośm')],
            'buttons' => ['1', '3'],
            'icons' => 'ok',
            'text' => 'foo',
            'requiredText' => 'bar',
            'email' => 'test@micrus.avris.it',
            'date' => '2016-02-01',
            'datetime' =>
                new \DateTime('2016-02-01 16:30:00.000000'),
            'time' => '16:30',
            'c3' => 'foobar',
            'multiWidget' => ['test@wp.pl', 'ok@gmail.com'],
            'multiSubForm' => [
                (object) ['email' => 'test1@ok.pl', 'choice' => 0],
                (object) ['email' => 'test2@ok.pl', 'choice' => 1],
            ],
            'subForm' => (object) ['email' => 'test3@ok.pl', 'choice' => 1],
        ], $obj);

        $this->assertCount(32, $form->iterate());
        $this->assertCount(5, $form->iterate(null, 'email'));
        $this->assertCount(3, $form->iterate('email', 'date'));
        $this->assertCount(7, $form->iterate('c1'));
    }
}
