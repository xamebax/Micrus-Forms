<?php
namespace Avris\Micrus\Forms;

class FormModuleTest extends \PHPUnit_Framework_TestCase
{
    public function testModule()
    {
        $module = new FormsModule();
        $config = $module->extendConfig('test', __DIR__);

        $this->assertEquals(
            ['assertLocaleSet'],
            array_keys($config['services'])
        );
    }
}
