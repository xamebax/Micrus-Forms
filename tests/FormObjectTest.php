<?php
namespace Avris\Micrus\Forms;

use App\Model\TestObjectApp;
use Avris\Micrus\Test\TestObject;
use Avris\Micrus\Test\TestObjectGen;

class FormObjectTest extends \PHPUnit_Framework_TestCase
{
    public function testFormObjectModel()
    {
        $obj = new FormObject(new TestObjectApp());
        $obj->getOriginal();
        $this->assertEquals('TestObjectApp', $obj->getName());
    }

    public function testFormObjectAttributes()
    {
        $obj = new FormObject();
        $obj->foo = 'bar';
        $this->assertEquals('bar', $obj->foo);
        $this->assertInstanceOf('\stdClass', $obj->getOriginal());
        $this->assertEquals('stdClass', $obj->getName());
    }

    public function testFormObjectAccessors()
    {
        $obj = new FormObject(new TestObject());
        $obj->foo = 'bar';
        $this->assertEquals('setbarget', $obj->foo);
        $this->assertTrue($obj->ok);

        $this->assertInstanceOf(TestObject::class, $obj->getOriginal());
        $this->assertEquals('Avris_Micrus_Test_TestObject', $obj->getName());
    }

    public function testFormObjectAccessorsManyString()
    {
        $obj = new FormObject(new TestObject());
        TestObject::$removeManyCount = 0;

        $this->assertFalse($obj->manyFoo);

        $obj->many = ['foo', 'bar'];
        $this->assertEquals(['foo', 'bar'], array_values($obj->many));
        $this->assertEquals(0, TestObject::$removeManyCount);

        $obj->many = ['foo', 'baz'];
        $this->assertEquals(['foo', 'baz'], array_values($obj->many));
        $this->assertEquals(1, TestObject::$removeManyCount);
        $this->assertTrue($obj->manyFoo);

        $obj->many = [];
        $this->assertEquals([], array_values($obj->many));
        $this->assertEquals(3, TestObject::$removeManyCount);
        $this->assertFalse($obj->manyFoo);
    }

    public function testFormObjectAccessorsManyObject()
    {
        $obj = new FormObject(new TestObject());
        TestObject::$removeManyCount = 0;

        $foo = (object) ['val' => 'foo'];
        $bar = (object) ['val' => 'bar'];
        $baz = (object) ['val' => 'baz'];

        $this->assertFalse($obj->manyFoo);

        $obj->many = [$foo, $bar];
        $this->assertEquals([$foo, $bar], array_values($obj->many));
        $this->assertEquals(0, TestObject::$removeManyCount);

        $obj->many = [$foo, $baz];
        $this->assertEquals([$foo, $baz], array_values($obj->many));
        $this->assertEquals(1, TestObject::$removeManyCount);
        $this->assertTrue($obj->manyFoo);

        $obj->many = [];
        $this->assertEquals([], array_values($obj->many));
        $this->assertEquals(3, TestObject::$removeManyCount);
        $this->assertFalse($obj->manyFoo);
    }

    public function testFormObjectGenericAccessors()
    {
        $obj = new FormObject(new TestObjectGen());
        $obj->foo = 'bar';
        $this->assertEquals('bar', $obj->foo);

        $this->assertInstanceOf(TestObjectGen::class, $obj->getOriginal());
        $this->assertEquals('Avris_Micrus_Test_TestObjectGen', $obj->getName());
    }

    public function testStatic()
    {
        $obj = new \stdClass();

        FormObject::set($obj, 'foo', 'bar');
        $this->assertEquals('bar', FormObject::get($obj, 'foo'));
    }
}
