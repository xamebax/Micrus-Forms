<?php
namespace Avris\Micrus\Forms\Style;

use Avris\Micrus\Forms\Widget\Widget;

class FormStyleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider styleClassProvider
     */
    public function testGetReplacements($class)
    {
        $widget = $this->getMockBuilder(Widget::class)->disableOriginalConstructor()->getMock();

        $class = __NAMESPACE__ . '\\' . $class;

        /** @var FormStyle $style */
        $style = new $class;
        $replacements = $style->getReplacements($widget);
        $this->assertArrayHasKey('{wrapper_before}', $replacements);
        $this->assertArrayHasKey('{wrapper_after}', $replacements);
        $this->assertArrayHasKey('{widget_before}', $replacements);
        $this->assertArrayHasKey('{widget_after}', $replacements);
        $this->assertArrayHasKey('{label_class}', $replacements);
        $this->assertArrayHasKey('{wrapper_class}', $replacements);
        $this->assertArrayHasKey('{widget_class}', $replacements);
    }

    public function styleClassProvider()
    {
        return [
            ['Bootstrap'],
            ['Bootstrap1'],
            ['Bootstrap2'],
            ['Bootstrap3'],
            ['BootstrapHalf'],
            ['BootstrapMini'],
            ['BootstrapInline'],
            ['BootstrapNoLabel'],
            ['BootstrapInlineNoLabel'],
        ];
    }
}