<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\BaseTest;

class ChoiceTest extends BaseTest
{
    public function testEmpty()
    {
        $choice = new Choice($this->form, 'name', ['model' => 'testie']);

        $this->assertNull($choice->valueFormToObject(''));
    }

    public function testEmptyMultiple()
    {
        $choice = new Choice($this->form, 'name', ['model' => 'testie', 'multiple' => true]);

        $this->assertSame([], $choice->valueFormToObject([]));
    }
}