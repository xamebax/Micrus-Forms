<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\BaseTest;

class ObjectValidatorTest extends BaseTest
{
    protected static $expectedHtml = <<<HTML
<html>
  <body>
    <div class="  form-general-errors">
        <div class=""/></div>
  </body>
</html>
HTML;


    public function testBasic()
    {
        $widget = new ObjectValidator($this->form, 'name', ['callback' => function() {}]);

        $this->assertEquals(
            static::$expectedHtml,
            $this->normalizeHtml((string) $widget)
        );
    }
}
