<?php
namespace Avris\Micrus\Forms\Widget;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\BaseTest;
use Avris\Micrus\Forms\Style\Bootstrap;

class WidgetTest extends BaseTest
{
    protected static $expectedRendered = <<<HTML
<html>
  <body>
    <div class="  ">
        <label for="stdClass_name" class="">Name</label><input id="stdClass_name" name="stdClass[name]" type="text" value="" class=""/></div>
  </body>
</html>
HTML;

    protected static $expectedRenderedBootstrap = <<<HTML
<html>
  <body>
    <div class="form-group  ">
      <div class="row">
        <div class="col-lg-12">
        <label for="stdClass_name" class="form-label">Name</label><input id="stdClass_name" name="stdClass[name]" type="text" value="" class="form-control"/></div>
      </div>
    </div>
  </body>
</html>
HTML;

    /** @var Text */
    protected $widget;

    protected function setUp()
    {
        parent::setUp();
        $this->widget = new Text($this->form, 'name', ['attr' => ['id' => 'wont be overwritten']]);
    }

    public function testRender()
    {
        $this->assertEquals(static::$expectedRendered, $this->normalizeHtml((string) $this->widget));
        $this->assertEquals(static::$expectedRendered, $this->normalizeHtml($this->widget->render()));
        $this->assertEquals(static::$expectedRenderedBootstrap, $this->normalizeHtml($this->widget->render(new Bootstrap())));
    }

    public function testRenderHide()
    {
        $widget = new Display($this->form, 'name', ['template' => function () { return false; }]);
        $this->assertSame('', $widget->render());
    }

    public function testOptions()
    {
        $this->widget->setOption('foo', 'bar');
        $this->assertEquals('bar', $this->widget->getOption('foo'));

        $this->assertFalse($this->widget->isReadonly());
        $this->widget->setOption('readonly', '1');
        $this->assertTrue($this->widget->isReadonly());
    }

    public function testValueObjectConversion()
    {
        $this->assertSame('foo', $this->widget->valueObjectToForm('foo'));
        $this->assertSame('foo', $this->widget->valueFormToObject('foo'));
    }

    public function testIsValidAccepting()
    {
        $assertAccepting = $this->getMockBuilder(Assert\Assert::class)->disableOriginalConstructor()->getMock();
        $assertAccepting->expects($this->any())->method('validate')->willReturn(true);

        $widget = new Text($this->form, 'name', [], [$assertAccepting]);

        $this->assertFalse($widget->isRequired());

        $this->assertTrue($widget->isValid(''));
        $this->assertSame('', $widget->getErrorsHtml());

        $this->assertTrue($widget->isValid('test'));
        $this->assertSame('', $widget->getErrorsHtml());
    }

    public function testIsValidRejecting()
    {
        $assertAccepting = $this->getMockBuilder(Assert\Assert::class)->disableOriginalConstructor()->getMock();
        $assertAccepting->expects($this->any())->method('validate')->willReturn(true);

        $assertRejecting = $this->getMockBuilder(Assert\Assert::class)->disableOriginalConstructor()->getMock();
        $assertRejecting->expects($this->any())->method('validate')->willReturn(false);

        $widget = new Text($this->form, 'name', [], [$assertAccepting, $assertRejecting]);

        $this->assertFalse($widget->isRequired());

        $this->assertTrue($widget->isValid(''));
        $this->assertSame('', $widget->getErrorsHtml());

        $this->assertFalse($widget->isValid('test'));
        $this->assertSame('<ul class="form-errors"><li>Validator.</li></ul>', $widget->getErrorsHtml());
    }

    public function testIsValidRequired()
    {
        $assertRequired = $this->getMockBuilder(Assert\NotBlank::class)->disableOriginalConstructor()->getMock();
        $assertRequired->expects($this->any())->method('validate')->willReturn(false);

        $widget = new Text($this->form, 'name', [], [$assertRequired]);

        $this->assertTrue($widget->isRequired());

        $this->assertFalse($widget->isValid(''));
        $this->assertSame('<ul class="form-errors"><li>Validator.</li></ul>', $widget->getErrorsHtml());
    }

    public function testIsValidDefault()
    {
        $widget = new DateTime($this->form, 'name');

        $this->assertTrue($widget->isValid(''));
        $this->assertSame('', $widget->getErrorsHtml());

        $this->assertFalse($widget->isValid('test'));
        $this->assertSame('<ul class="form-errors"><li>Validator.DateTime</li></ul>', $widget->getErrorsHtml());

        $this->assertFalse($widget->isValid('2015-01-02'));
        $this->assertSame('<ul class="form-errors"><li>Validator.DateTime</li></ul>', $widget->getErrorsHtml());

        $this->assertTrue($widget->isValid('2015-01-02 08:00'));
        $this->assertSame('', $widget->getErrorsHtml());
    }

    public function testIsValidDefaultOverwritten()
    {
        $widget = new DateTime($this->form, 'name', [], [new Assert\DateTime('foo')]);

        $this->assertTrue($widget->isValid(''));
        $this->assertSame('', $widget->getErrorsHtml());

        $this->assertFalse($widget->isValid('test'));
        $this->assertSame('<ul class="form-errors"><li>Foo</li></ul>', $widget->getErrorsHtml());

        $this->assertFalse($widget->isValid('2015-01-02'));
        $this->assertSame('<ul class="form-errors"><li>Foo</li></ul>', $widget->getErrorsHtml());

        $this->assertTrue($widget->isValid('2015-01-02 08:00'));
        $this->assertSame('', $widget->getErrorsHtml());
    }
}