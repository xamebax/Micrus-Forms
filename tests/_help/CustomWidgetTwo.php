<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Forms\Widget\Widget;

class CustomWidgetTwo extends Widget
{
    protected $template = '<div class="c2"></div>';
}
