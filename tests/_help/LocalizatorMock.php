<?php
namespace Avris\Micrus\Forms\Widget;

function l($word) {
    return ucfirst(preg_replace('#^entity\..*\..*\.(.*)$#U', '$1', $word));
}
