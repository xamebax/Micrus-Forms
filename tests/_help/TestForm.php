<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Style\Bootstrap2;
use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Form;

class TestForm extends Form
{
    public function configure()
    {
        $choices = [1 => 'jeden', 2 => 'dwa', 3 => 'trzy'];

        $this
            ->with('Standard')
                ->add('text')
                ->add('text2')
                ->add('requiredText', Widget\Text::class, [], new Assert\NotBlank)
                ->add('invisible', null, [], [], false)
                ->add('email', 'Email', ['placeholder' => 'user@domain.eu'], [new Assert\NotBlank, new Assert\Email])
                ->add('agree', 'Checkbox')
                ->add('date', 'Date', ['class' => 'datetimepicker date-big'])
                ->add('datetime', 'DateTime')
                ->add('time', 'Time')
                ->add('file', 'File')
                ->add('number', 'Number')
                ->add('int', 'Integer')
                ->add('textAddon', 'TextAddon', ['before' => '$'])
                ->add('numberAddon', 'NumberAddon', ['after' => '€'])
                ->add('url', 'Url')
                ->add('isCustomlyValid', 'ObjectValidator')
            ->end()
            ->with('Choice')
                ->add('choiceEmpty', 'Choice')
                ->add('choiceSN', 'Choice', [
                    'choices' => $choices,
                    'add_empty' => true,
                ])
                ->add('choiceMN', 'Choice', [
                    'choices' => $choices,
                    'multiple' => true,
                    'choiceTranslation' => 'prefix.',
                ])
                ->add('choiceSE', 'Choice', [
                    'choices' => $choices,
                    'expanded' => true,
                ])
                ->add('choiceME', 'Choice', [
                    'choices' => $choices,
                    'multiple' => true,
                    'expanded' => true,
                ])
                ->add('choiceModel', 'Choice', [
                    'model' => 'Testie',
                ])
                ->add('choiceModelM', 'Choice', [
                    'model' => 'Testie',
                    'multiple' => true,
                ])
                ->add('buttons', 'ButtonChoice', [
                    'choices' => $choices,
                    'multiple' => true,
                    'class' => 'btn-primary btn-sm',
                ])
                ->add('icons', 'IconChoice', [
                    'choices' => [
                        'ok' => 'fa fa-check',
                        'nope' => 'fa fa-cross',
                    ],
                ])
            ->end()
            ->with('Custom')
                ->add('c1', 'CustomWidgetOne')
                ->add('c2', 'Avris\Micrus\Test\CustomWidgetTwo')
                ->add('c3', 'Display', [
                    'label' => '',
                    'template' => function($value) {
                        return $value
                            ? '<strong>' . $value . '</strong>'
                            : false;
                    },
                ])
                ->add('c4', 'Display', [
                    'label' => '',
                    'template' => '<em>{value}</em>',
                ])
                ->add('invisible', 'Text', [], [], $this->options->get('isAdmin'))
            ->end()
            ->with('Advanced')
                ->add('multiWidget', Widget\MultipleWidget::class, [
                    'widget' => Widget\Email::class,
                    'widgetOptions' => [
                        'placeholder' => 'user@domain.eu',
                    ],
                    'widgetAsserts' => [
                        new Assert\NotBlank,
                    ],
                    'add' => true,
                    'btnAddText' => '<span class="fa fa-plus-circle"></span>',
                    'remove' => true,
                    'btnRemoveText' => '<span class="fa fa-trash"></span>',
                ], [
                    new Assert\NotBlank(),
                    new Assert\MinCount(1),
                    new Assert\MaxCount(10),
                ])
                ->add('multiSubForm', Widget\MultipleSubForm::class, [
                    'form' => TestSubForm::class,
                    'model' => '\stdClass',
                    'container' => $this->container,
                    'add' => true,
                    'btnAddText' => '<span class="fa fa-plus-circle"></span>',
                    'remove' => true,
                    'btnRemoveText' => '<span class="fa fa-trash"></span>',
                ], [
                    new Assert\UniqueField('email'),
                    new Assert\NotBlank(),
                    new Assert\MinCount(1),
                    new Assert\MaxCount(10),
                ])
                ->add('subForm', Widget\SubForm::class, [
                    'form' => TestSubForm::class,
                    'model' => \stdClass::class,
                    'container' => $this->container,
                    'style' => Bootstrap2::class
                ])
            ->end()
        ;
    }

    public function isCustomlyValid()
    {
        return true;
    }

    public function triggerInvalidWidget()
    {
        $this->add('foo', 'nonexistent');
    }

    public function triggerDuplicateWidget()
    {
        $this->add('text');
    }

    public function triggerInvalidObjectValidator()
    {
        $this->add('foo', 'ObjectValidator', ['callback' => 'text']);
    }

}
