<?php
namespace Avris\Micrus\Test;

class TestObject
{
    private $foo;

    public $bar;

    private $many = [];

    public static $removeManyCount = 0;

    public $public = 'PUBLIC';

    public function setFoo($foo)
    {
        $this->foo = 'set' . $foo;
    }

    public function getFoo()
    {
        return $this->foo . 'get';
    }

    public function getLol()
    {
        return 'xD';
    }

    public function isOk()
    {
        return true;
    }

    public function getTemplateDirs()
    {
        return [];
    }

    public function getMany()
    {
        return $this->many;
    }

    public function addMany($val)
    {
        $this->many[] = $val;
    }

    public function removeMany($val)
    {
        self::$removeManyCount++;

        foreach ($this->many as $i => $currentVal) {
            if ($currentVal == $val) {
                unset($this->many[$i]);
            }
        }
    }

    public function hasManyFoo()
    {
        return count($this->many) > 0;
    }
}
