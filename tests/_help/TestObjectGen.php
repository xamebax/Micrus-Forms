<?php
namespace Avris\Micrus\Test;

class TestObjectGen
{
    public $values = [];

    public function set($key, $value)
    {
        $this->values[$key] = $value;
    }

    public function get($key)
    {
        return isset($this->values[$key])
            ? $this->values[$key]
            : null;
    }

    public function has($key)
    {
        return isset($this->values[$key]);
    }

    public function is($key)
    {
        return isset($this->values[$key]) && $this->values[$key];
    }
}
