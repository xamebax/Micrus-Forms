<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;

class TestSubForm extends Form
{
    public function configure()
    {
        $this
                ->add('email', 'Email', ['placeholder' => 'user@domain.eu'], [new Assert\NotBlank, new Assert\Email])
                ->add('choice', 'Choice', [
                    'choices' => [0 => 'zero', 1 => 'jeden'],
                ])
            ->end();
    }
}
