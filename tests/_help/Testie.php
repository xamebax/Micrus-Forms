<?php
namespace App\Model;

class Testie
{
    protected $id;

    protected $value;

    public function __construct($id = 0, $value = null)
    {
        $this->id = $id;
        $this->value = $value;
    }

    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string) $this->value;
    }

}
